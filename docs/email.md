# Setup Email on iOS

1. Goto Settings
1. Select Mail, Contacts, Calendars
1. Select Add Account
1. Select Other
1. Select Add Mail Account
1. Enter your account information and tap Next
1. Select IMAP
1. Enter mail.yourdomain.com with your username and password
1. Disable the Notes toggle and tap Save

# Setup Email on Android
1. Tap the Applications icon
1. Tap Email
1. Enter your email address and password, and tap Manual setup
1. Tap IMAP
1. Enter your account information and tap Next 
1. Enter mail.yourdomain.com with your username and password
1. Select your email preferences and tap Next
1. Enter your account and display names, and tap Next
